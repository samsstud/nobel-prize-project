import numpy as np
from ase.lattice.cubic import FaceCenteredCubic
from pyiron_atomistics.atomistics.structure.atoms import ase_to_pyiron
from elaston import LinearElasticity
from sklearn.cluster import AgglomerativeClustering
from nobel_prize_project.src.base import Lattice, ProjectContainer
from nobel_prize_project.src.linear_elasticity import get_elastic_constants
import itertools


def get_elastic_tensor_by_orientation(orientation, elastic_tensor):
    """
    Get elastic tensor in a given orientation
    Args:
        orientation (numpy.ndarray): 3x3 orthogonal orientation (no need to be orthonormal)
        elastic_tensor (numpy.ndarray): 6x6 elastic tensor
    Returns:
        (numpy.ndarray) 6x6 elastic_tensor in a given orientation
    """
    orientation = np.einsum(
        "ij,i->ij", orientation, 1 / np.linalg.norm(orientation, axis=-1)
    )
    if not np.isclose(np.linalg.det(orientation), 1):
        raise ValueError("orientation must be an orthogonal 3x3 tensor")
    C = np.zeros((3, 3, 3, 3))
    r = np.arange(3)
    for i, j, k, l in itertools.product(r, r, r, r):
        C[i, j, k, l] = elastic_tensor[
            i + (i != j) * (6 - 2 * i - j), k + (k != l) * (6 - 2 * k - l)
        ]
    C = np.einsum(
        "Ii,Jj,ijkl,Kk,Ll->IJKL", orientation, orientation, C, orientation, orientation
    )
    elastic_tensor_to_return = np.zeros_like(elastic_tensor)
    for i, j, k, l in itertools.product(r, r, r, r):
        elastic_tensor_to_return[
            i + (i != j) * (6 - 2 * i - j), k + (k != l) * (6 - 2 * k - l)
        ] = C[i, j, k, l]
    return elastic_tensor_to_return


class Dislocation(ProjectContainer):
    def __init__(self, pr, mode='edge'):
        self.pr = pr
        self.mode = mode
        self._labels = None

    @property
    def orientation(self):
        if self.mode=='edge':
            return [(-1, 1, 0), (1, 1, 1), (1, 1, -2)]
        else:
            return [(1, 1, -2), (1, 1, 1), (1, -1, 0)]

    def get_elastic_tensor(self):
        return get_elastic_tensor_by_orientation(self.orientation, get_elastic_constants(pr=self.pr))

    def get_slab(self, target_boxsize, buffer=10):
        slab = FaceCenteredCubic(
            directions=self.orientation,
            size=(1, 1, 1), symbol='Ni', pbc=False
        )
        slab = ase_to_pyiron(slab).repeat(np.rint(target_boxsize / slab.cell.diagonal()).astype(int))
        slab.pbc[-1] = True
        slab.positions[:, :2] += 0.5 * buffer
        slab.cell[0, 0] += buffer
        slab.cell[1, 1] += buffer
        return slab

    def displace_atoms(self, slab):
        medium = LinearElasticity(self.get_elastic_tensor())
        voro = slab.analyse.get_voronoi_vertices()[:, :2]
        x_core = voro[np.linalg.norm(voro - 0.5 * slab.cell.diagonal()[:2], axis=-1).argmin()]
        slab.positions += medium.get_dislocation_displacement(
            slab.positions[:, :-1] - x_core, self.burgers_vector
        )
        return slab

    @property
    def a_0(self):
        lattice = Lattice(self.pr)
        return lattice.a_0

    @property
    def burgers_vector(self):
        b = self.a_0 / np.sqrt(2)
        if self.mode == 'edge':
            return np.asarray([b, 0, 0])
        else:
            return np.asarray([0, 0, b])

    def set_selective_dynamics(self, slab, freeze_area):
        outer_atoms = slab.positions[
            slab.get_neighbors(num_neighbors=None, cutoff_radius=0.95 * self.a_0).numbers_of_neighbors < 11
        ]
        outer_neighborhood = slab.get_neighborhood(outer_atoms, num_neighbors=None, cutoff_radius=freeze_area)
        slab.add_tag(selective_dynamics=[True, True, True])
        slab.selective_dynamics[outer_neighborhood.flattened.indices] = [False, False, False]
        return slab

    def _get_job(self, target_boxsize=np.array([100, 100, 10]), buffer=10, freeze_area=10):
        slab = self.get_slab(target_boxsize=target_boxsize, buffer=buffer)
        slab = self.displace_atoms(slab)
        slab = self.set_selective_dynamics(slab, freeze_area=freeze_area)
        lmp = self.get_job(slab, minimize=True)
        return lmp

    def get_structure(self, target_boxsize=np.array([100, 100, 10]), buffer=10, freeze_area=10):
        lmp = self._get_job(target_boxsize=target_boxsize, buffer=buffer, freeze_area=freeze_area)
        return lmp.get_structure()

    def get_energy(self, target_boxsize=np.array([100, 100, 10]), buffer=10, freeze_area=10):
        lmp = self._get_job(target_boxsize=target_boxsize, buffer=buffer, freeze_area=freeze_area)
        return lmp.output.energy_pot[-1]

    @property
    def labels(self):
        if self._labels is None:
            structure = self.get_structure()
            cna_str = structure.analyse.pyscal_cna_adaptive(mode='str')
            agg = AgglomerativeClustering(n_clusters=None, distance_threshold=10, linkage='single')
            labels = agg.fit_predict(structure.positions[cna_str=='others'])
            border, core_one, core_two = np.unique(labels)
            others = np.where(cna_str=='others')[0]
            cna_str[others[labels==border]] = 'border'
            cna_str[others[labels==core_one]] = 'p_one'
            cna_str[others[labels==core_two]] = 'p_two'
            self._labels = cna_str
        return self._labels
