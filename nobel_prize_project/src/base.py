import numpy as np
from hashlib import sha1
from pyiron_contrib.atomistics.lammps.drag import setup_lmp_input
from typing import Optional


def get_strain(temperature):
    T = np.atleast_1d(temperature)
    coeff = np.array([1.34773582e-05, 3.14721479e-10, 9.11974576e-13])
    return np.einsum("i,i...->...", coeff, [T, T**2, T**3]).squeeze()[()]


def get_job(
    pr, structure, job_name=None, minimize=False, pressure=None, drag_fix_id=None, run=True, drag_direction=None
):
    """
    Get a lammps job for a given structure. If the job is already finished, return the job object.
        
    Args:
        pr: pyiron Project object
        structure: pyiron Structure object
        job_name: str, optional
        minimize: bool, optional
        pressure: float, optional
        drag_fix_id: int, optional
        run: bool, optional
        drag_direction: str, optional

    Returns:
        pyiron Job object
    """
    job_name = 'lmp_' + sha1(
        (structure.__repr__() + str(minimize) + str(pressure) + str(drag_fix_id) + str(job_name)).encode()
    ).hexdigest()
    lmp = pr.create.job.Lammps(job_name)
    if lmp.status.finished:
        return lmp
    lmp.potential = get_potential()
    lmp.structure = structure
    if minimize:
        lmp.calc_minimize(pressure=pressure)
    if drag_fix_id is not None:
        setup_lmp_input(lmp, drag_fix_id, direction=drag_direction)
    if run:
        lmp.run()
    return lmp


class ProjectContainer:
    def __init__(self, pr):
        self.pr = pr

    def get_job(self, structure, job_name=None, minimize=False, pressure=None, drag_fix_id=None, run=True, drag_direction=None):
        return get_job(
            pr=self.pr,
            structure=structure,
            job_name=job_name,
            minimize=minimize,
            pressure=pressure,
            drag_fix_id=drag_fix_id,
            run=run,
            drag_direction=drag_direction,
        )


def get_potential():
    return '1995--Angelo-J-E--Ni-Al-H--LAMMPS--ipr1'

class Lattice(ProjectContainer):
    def __init__(self, pr, temperature=0):
        super().__init__(pr=pr)
        self._temperature = temperature
        self._energy_Ni = None

    def get_bulk_job(self, pressure=0, minimize=True):
        return self.get_job(
            self.unrelaxed_structure,
            minimize=minimize,
            pressure=pressure
        )

    @property
    def unrelaxed_structure(self):
        return self.pr.create.structure.bulk('Ni', cubic=True)

    @property
    def structure(self):
        lmp = self.get_bulk_job(pressure=0, minimize=True)
        return lmp.get_structure().apply_strain(
            get_strain(self._temperature), return_box=True
        )

    @property
    def energy(self):
        if self._energy_Ni is None:
            lmp = self.get_bulk_job(pressure=0, minimize=True)
            self._energy_Ni = lmp.output.energy_pot[-1] / len(lmp.structure)
        return self._energy_Ni

    @property
    def a_0(self):
        lmp = self.get_bulk_job(pressure=0)
        return lmp.get_structure().cell[0, 0] * (1 + get_strain(self._temperature))

class Bulk(Lattice):
    def __init__(self, n_repeat, pr, temperature=0):
        super().__init__(pr=pr, temperature=temperature)
        self._n_repeat = n_repeat
        self._structure = None

    @property
    def structure(self):
        if self._structure is None:
            self._structure = super().structure.repeat(self._n_repeat)
        return self._structure.copy()

    def get_interstitials(self, int_type):
        voro = self.structure.analyse.get_voronoi_vertices()
        neigh = self.structure.get_neighborhood(voro, num_neighbors=1)
        d_mean = np.mean(neigh.distances)
        if int_type == 'octa':
            return voro[neigh.distances.flatten() > d_mean]
        elif int_type == 'tetra':
            return voro[neigh.distances.flatten() < d_mean]
        else:
            raise ValueError('interstitial type not recognized')

    @property
    def x_octa(self):
        return self.get_interstitials('octa')

    @property
    def x_tetra(self):
        return self.get_interstitials('tetra')

    @property
    def x_trans(self):
        return np.array([1, 1, 2]) / 6 * self.a_0

    def add_hydrogen(self, positions, repeat=1):
        x = np.atleast_2d(positions).reshape(-1, 3)
        return (
            self.structure.repeat(repeat)
            + self.pr.create.structure.atoms(
                positions=x, elements=len(x) * ['H'], cell=self.structure.cell
            )
        ).center_coordinates_in_unit_cell()


class Hydrogen(Bulk):
    def _get_energy(self, job, E_ref=None):
        if E_ref is None:
            E_ref = self.E_octa
        E = job.output.energy_pot
        E -= len(job.structure.select_index('Ni')) * self.energy
        E -= len(job.structure.select_index('H')) * E_ref
        return E

    def _get_distance_from_center(self, x):
        return x - self.structure.cell.diagonal() / 2

    def get_center_position(self, x):
        index = np.linalg.norm(self._get_distance_from_center(x), axis=-1).argmin()
        return x[index]

    @property
    def _lmp_octa(self):
        return self.get_job(self.add_hydrogen(self.get_center_position(self.x_octa)))

    @property
    def _lmp_tetra(self):
        return self.get_job(self.add_hydrogen(self.get_center_position(self.x_tetra)))

    @property
    def _lmp_trans(self):
        structure = self.add_hydrogen(self.x_trans)
        return self.get_job(structure, drag_fix_id=structure.select_index("H")[0], minimize=True)

    @property
    def E_octa(self):
        return self._get_energy(self._lmp_octa, E_ref=0)[-1]

    @property
    def E_tetra(self):
        return self._get_energy(self._lmp_tetra)[-1]

    @property
    def E_trans(self):
        return self._get_energy(self._lmp_trans)[-1]
