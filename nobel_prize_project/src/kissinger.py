import numpy as np
from scipy.special import expi


def get_kissinger(t, A, energy, rate, T_start, kB=8.617333262e-5):
    xi_0 = energy / (kB * T_start)
    xi_1 = energy / (kB * (T_start + rate * t))
    aux0=((((-A*(energy*(expi(xi_1)))))/(kB))/rate)-(((A*(energy*(expi(xi_0))))/(kB))/rate)
    aux1=A*((((np.exp(xi_1))*((rate*t)+T_start))/rate)-(((energy*(expi(xi_1)))/(kB))/rate))
    aux2=((A*((np.exp(xi_1))*((rate*t)+T_start)))/rate)+(((A*(energy*(expi(xi_0))))/(kB))/rate)
    aux3=np.exp((((A*((np.exp(xi_0))*T_start))/rate)+(((A*(energy*(expi(xi_1))))/(kB))/rate)))
    output=(np.exp((aux0-aux1)))*((np.exp(aux2))-aux3);
    return output
    
