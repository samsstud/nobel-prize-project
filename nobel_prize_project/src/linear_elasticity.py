import numpy as np
from pint import UnitRegistry
from nobel_prize_project.src.base import Bulk, get_potential, Lattice, ProjectContainer, Hydrogen
from sklearn.neighbors import KNeighborsRegressor


class Dipole(Hydrogen):
    def __init__(self, n_repeat, pr):
        super().__init__(pr=pr, n_repeat=n_repeat)
        self.ureg = UnitRegistry()

    @property
    def GPaA3(self):
        return self.ureg.angstrom**3 * 1e9 * self.ureg.pascal

    def _lmp_to_dipole(self, lmp):
        return (lmp.output.volume[-1] * lmp.output.pressures[-1] * self.GPaA3).to('eV').magnitude

    @property
    def octa(self):
        return self._lmp_to_dipole(self._lmp_octa)

    @property
    def tetra(self):
        return self._lmp_to_dipole(self._lmp_tetra)

    @property
    def trans(self):
        return self._lmp_to_dipole(self._lmp_trans)


class Kanzaki(ProjectContainer):
    def __init__(self, n_repeat, pr, cutoff_radius=5, f_min=1.0e-2):
        self.pr = pr
        self.dipole = Dipole(n_repeat=n_repeat, pr=pr)
        self._relaxed = True
        self._job = None
        self._structure = None
        self._neigh = None
        lattice = Lattice(pr)
        self.cutoff_radius = cutoff_radius * lattice.a_0
        self._displacements = None
        self._x_H = None
        self.f_min = f_min
        self._kanzaki_forces = None
        self._regressor = None

    @property
    def regressor(self):
        if self._regressor is None:
            self._regressor = [KNeighborsRegressor(n_neighbors=1) for _ in range(3)]
            for ii, ff in enumerate(self.forces.T):
                self._regressor[ii].fit(self.dx, ff)
        return self._regressor

    def get_force(self, x):
        x = np.asarray(x).reshape(-1, 3)
        return np.squeeze([self.regressor[ii].predict(x) for ii in np.arange(3)]).T

    @property
    def relaxed(self):
        return self._relaxed

    @relaxed.setter
    def relaxed(self, boolean):
        self._job = None
        self._relaxed = boolean
        self._kanzaki = None

    @property
    def lmp_kanzaki(self):
        lmp = self.dipole.lmp_dipole
        return self.get_job(
            lmp.get_structure()[lmp.structure.select_index('Ni')],
        )

    @property
    def job(self):
        if self._job is None:
            if self.relaxed:
                 self._job = self.lmp_kanzaki
            else:
                 self._job = self.dipole.lmp_dipole
        return self._job

    @property
    def forces(self):
        f = self.job.output.forces[0, self.job.structure.select_index('Ni')]
        if not self.relaxed:
            f = -f
        return f

    @property
    def f_atoms(self):
        return np.linalg.norm(self.forces, axis=-1) > self.f_min

    @property
    def kanzaki_forces(self):
        if self._kanzaki_forces is None:
            self._kanzaki_forces = {
                'positions': self.positions[self.f_atoms],
                'forces': self.forces[self.f_atoms]
            }
        return self._kanzaki_forces

    @property
    def positions(self):
        return self.job.output.positions[0, self.job.structure.select_index('Ni')]

    @property
    def dx(self):
        return self.job.structure.find_mic(self.positions - self.x_H)

    @property
    def structure(self):
        if self._structure is None:
            self._structure = self.dipole.lmp_dipole.structure
            self._structure = self._structure[self._structure.select_index('Ni')]
        return self._structure.copy()

    @property
    def x_H(self):
        if self._x_H is None:
            job = self.dipole.lmp_dipole
            self._x_H = job.structure.positions[job.structure.select_index('H')[0]]
        return self._x_H

    @property
    def neigh(self):
        if self._neigh is None:
            self._neigh = self.structure.get_neighborhood(
                self.x_H, num_neighbors=None, cutoff_radius=self.cutoff_radius
            )
        return self._neigh

    @property
    def displacements(self):
        if self._displacements is None:
            lmp = self.dipole.lmp_dipole
            self._displacements = lmp.output.total_displacements[-1, lmp.structure.select_index('Ni')]
        return self._displacements


def get_elastic_job(pr):
    lmp = pr.create.job.Lammps('lmp_elast')
    lmp.structure = Lattice(pr).structure
    lmp.potential = get_potential()
    lmp.interactive_open()
    elast = lmp.create_job('ElasticMatrixJob', 'elast')
    if elast.status.initialized:
        elast.run()
    return elast


def get_elastic_constants(pr):
    ureg = UnitRegistry()
    elast = get_elastic_job(pr=pr)
    return (
        elast["output/elasticmatrix"]["elastic_matrix"] * 1e9 * ureg.pascal
    ).to(ureg.electron_volt / ureg.angstrom**3).magnitude
