import numpy as np
from nobel_prize_project.src.base import get_potential, Lattice, Bulk, ProjectContainer, Hydrogen
from tqdm.auto import tqdm
from functools import cached_property


class InterstitialInteractions(Hydrogen):
    def _run_job(self, positions, job_name, minimize=True, drag_fix_id=None):
        E_list = []
        for xx in positions:
            lmp = self.get_job(
                structure=self.add_hydrogen(xx),
                minimize=minimize,
                pressure=None,
                drag_fix_id=drag_fix_id,
            )
            E_list.append(self._get_energy(lmp))
        return np.array(E_list)

    def get_unique_positions(self, x_ref, positions):
        d = self.add_hydrogen(x_ref).get_distances_array(x_ref, positions).squeeze()
        x_octa_unique = positions[np.unique(np.round(d, decimals=3), return_index=True)[1]]
        return np.einsum('ijk->jik', [np.tile(x_ref, len(x_octa_unique)).reshape(-1, 3), x_octa_unique])

    @cached_property
    def E_octa_octa(self):
        x = self.get_unique_positions(self.x_octa[0], self.x_octa[1:])
        E_list = self._run_job(x, 'octa_octa')
        return {
            'dx': lmp.structure.find_mic(np.diff(x, axis=1)).squeeze(),
            'energy': E_list,
        }

    @cached_property
    def E_octa_tetra(self):
        x = self.get_unique_positions(self.x_tetra[0], self.x_octa)
        E_list = self._run_job(x, 'octa_tetra')
        return {
            'dx': lmp.structure.find_mic(np.diff(x, axis=1)).squeeze(),
            'energy': E_list,
        }

    @cached_property
    def E_trans_octa(self):
        x = self.get_unique_positions(self.x_trans, self.x_octa)
        E_list = self._run_job(x[1:], 'trans_octa', minimize=False)
        return {
            'dx': lmp.structure.find_mic(np.diff(x[1:], axis=1)).squeeze(),
            'energy': E_list,
        }
