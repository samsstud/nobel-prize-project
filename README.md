# Hydrogen diffusion in nickel and thermal desorption spectroscopy

## Hydrogen diffusion in dislocations

- Linear elasticity and collective H-H interactions

## Study of H energy surface in and around grain boundaries using metadynamics

- Comparison between
  - Kinetic study using diffusion equation
  - Metadynamics
  - Molecular dynamics

You can [use this repository as it is](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fsamsstud%2Fnobel-prize-project.git/HEAD) or fork it, for example specifying the environment.
