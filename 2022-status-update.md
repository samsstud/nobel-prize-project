# Diffusion of H in Ni grain boundaries

## What we did / achieved

- **One** Sigma 5 grain boundary in Ni
- MD simulations with various numbers of H (small GB box)
  - Diffusivity of H (single H)
  - Segregation of H in GB -> jump from GB
  - Correlation of GB & H motion?
  - Still substantial amount of data to evaluate (for multiple H no evaluation yet)
  - H was very mobile even for low T (~500/600 K)
- Static calculations of pairwise H-H interactions
  - Nearest neighbor H-H interactions were relatively small
- kMC for 1 H
  - Detection of interstitial sites -> some sites look like 2 sites
  - Creation of graph structure for kMC
  - Calculation of migration energies via ART
- Comparison between MD and kMC
  - Diffusivity in and out of the plane
  - Distance between GB was not the same -> MD faster perpendicular to GB
  - Diffusivity in the plane was surprisingly similar
  - Maybe non-Arrhenius behaviour?
- Dislocation
  - Linear elasticity gives energy barriers in most of the sites (~1nm from core)
  - H-H interactions are more complicated than pairwise interactions
    - Collective interactions to be investigated
- Grain boundary
  - Metadynamics for the full free energy surface
    - Parameter studies for metadynamics
  - Diffusion equation using free energy surface
  - Solving master equations
    - Temperature dependence cannot be included (maybe we don't need it?)
    - Eigenvectors to the defective matrix were not linearly independent (this could be a problem of numpy)
- Non-arrhenius behaviour of H diffusion
  - Metadynamics to study free energy surface for temperature-dependent energy barrier
- Theoretical calculation
  - Ali's Kissinger equation
  - Deconvolution of TDS, which doesn't work with standard Gaussian distribution (which experimentalists do)

## Short-term to do list

- Solve master equations numerically
  - Do TDS with GB (regardless of whether we have all barriers)
  - Aditya: Calculate energy barriers
  - Comparison with MD
  - Parametrization of collective H-H interactions -> dislocations
- Metadynamics
  - Ali: Parameter studies of metadynamics
  - Free energy of H around vacancies, GB -> continuum diffusion equation
  - For Silvia: Density of states of GB
- MD
  - Free surface MD -> study vacancy formation energy as a function of H
  - Combine the results with metadynamics & static calculations
  - Observe vacancy-H motions using high T MD
  - Make a list of all observations
  - Study with static calculations



## Long-term perspectives

- [ ] Extension to more realistic grain boundary density (strain dependence?)
- [ ] H-B interactions at grain boundaries in Fe
- [ ] Grain boundary motion due to H
  - Are GB islands stabilized by H?
  - Extension of Sherri's work (?)
  - Tilmann: problem too complex

## Project timetable / milestones

- (2021/03/21): TDS with kMC
  - Continuum solution?
  - Eunan's previous calculation
- (2021/03/26):
  - Metadynamics of H segregation
- (2021/04/16):
  - TDS with kMC H for GB
  - Analysis of MD data for multiple H -> No change (?) in diffusivity
- (2021/04/23):
  - TDS spectrum for various T
- (2021/05/07):
  - Diffusion with high H -> better statistics to see H-H effects ?
  - TDS with vacancies
- (2021/05/14):
  - TDS with vacancies
- (2021/05/21):
  - Evaluation of vacancy results
- (2021/05/28):
  - First TDS spectrum!!! for vacancy (Sam's ineptitude)
  - (Show Gaussian fitting can give rise to physically incorrect interpretations)
- (2021/06/04):
  - More correct TDS spectrum!!! (Ali's correct calculation)
- (2021/06/11):
  - Even more correct TDS spectrum!!! (Include heating rate inside kMC)
- (2021/06/18):
  - Creation of a straight edge dislocation in Ni (now we can easily have screw) + decomposition into partials!!!
  - Yuriy's TDS spectrum for single crystalline Ni with and without strain
- (2021/06/25)
  - Calculate diffusion barriers in & around dislocations + stacking fault
- (2021/07/01)
  - FCC - HCP interface
- (2021/07/08)
  - Investigation of partial dislocation core structure -> H stay in octahedral sites
  - H binding energies follow strain field -> comparison with linear elasticity theory
- (2021/07/15)
  - Linear elasticity -> Valid (i.e. error < 1 meV in octahedral sites) in range > 1.8 nm
  - Dataset creation of H-H interactions
- (2021/07/22)
  - H-H interactions for octahedral sites (Box possibly too small)
    - 1st & 2nd shell: attractive; 3rd and more: repulsive. 2nd order terms: little contribution
- (2021/07/29)
  - H-H interactions depend on local H concentration (frustration)
  - H-H-H interactions (finding triangles!)
- (2021/08/12)
  - Creation of general graph structure for H diffusion (superposed kMC lattice structure)
- (2021/08/19)
  - Comparison between analytical & kMC diffusion
  - How to count the "number of possible paths", counting saddle points or destinations?
- (2021/08/26)
  - kMC with H-H interactions (how to discussion)
- (2021/09/16)
  - H-H interactions for tetra-octa
  - Consider H-H interactions around dislocations (parametrization of H-H with strain field?) cf. work of Gerard
  - Change attempt frequency to escape time (to use more realistic heating rate)
  - Change system size
  - Change vacancy concentration -> How to translate theory to experiment?
  - Comparison of Gaussian and "analytical" solution
  - Extend bulk area
  - Define chemical potential properly
  - kMC with multiple H -> Comparison with MD
  
## Organizational

- Every Thursday at 2pm for 2h
- Think of presenting at H sessions
